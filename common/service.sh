#!/system/bin/sh
# Please don't hardcode /magisk/modname/... ; instead, please use $MODDIR/...
# This will make your scripts compatible even if Magisk change its mount point in the future
MODDIR=${0%/*}

# This script will be executed in late_start service mode
# More info in the main Magisk thread
# Disable keyboard backlight
echo 0 > /sys/devices/soc.0/leds-qpnp-21/leds/button-backlight/max_brightness # Max Brightness to 0
echo 0 > /sys/devices/soc.0/leds-qpnp-21/leds/button-backlight/brightness # Current brightness to 0
